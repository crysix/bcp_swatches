<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_BCP_Swatches
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_BCP_Swatches_Block_Swatches extends Mage_Core_Block_Template
{
	/**
	 * Set the caching parameters
	 */
	protected function _construct()
	{
		parent::_construct();
		if (! ($ttl = (int) Mage::helper('bcp_swatches')->getConfig('swatches_block_cache_ttl')))
		{
			$ttl = false;
		}
		$this->addData(array(
			'cache_lifetime' => $ttl,
			'cache_key' => 'BCP_swatches_' . Mage::app()->getStore()->getId() . '_' . $this->getProduct()->getId(),
			'cache_tags' => array(Mage_Catalog_Model_Product::CACHE_TAG),
		));
	}

	/**
	 * Get the array of simple product swatches.
	 * The size of the swatch image can be specified as parameters.
	 *
	 * @param int $width
	 * @param int $height
	 * @return array
	 */
	public function getSwatches($width = null, $height = null)
	{
		if (null === $this->getData('swatches'))
		{
			$swatches = array();
			$product = $this->getProduct();
			if ($product->isConfigurable())
			{
				if (is_null($width))
				{
					$width = Mage::helper('bcp_swatches')->getConfig('swatch_image_width');
				}
				if (is_null($height))
				{
					$height = Mage::helper('bcp_swatches')->getConfig('swatch_image_height');
				}

				$collection = $this->_getSimpleProductCollection($product);
				foreach ($collection as $simpleProduct)
				{
					Mage::helper('bcp_swatches')->setSwatchImageOnProduct($simpleProduct);

					if ($swatch = $this->_getSwatchDataFromProduct($simpleProduct, $width, $height))
					{
						$swatches[] = $swatch;
					}
				}

				if (Mage::helper('bcp_swatches')->getConfig('sort_by'))
				{
					uasort($swatches, array(__CLASS__, 'cmp_swatches'));
				}
			}
			$this->setData('swatches', $swatches);
		}
		return $this->getData('swatches');
	}

	/**
	 * Compare two Swatch Objects according to the system configuration.
	 * Used for the sorting of the swatch images.
	 *
	 * @param Varien_Object $a
	 * @param Varien_Object $b
	 * @return int
	 */
	static public function cmp_swatches($a, $b)
	{
		$field = Mage::helper('bcp_swatches')->getConfig('sort_by');
		$aValue = $a->getData($field);
		$bValue = $b->getData($field);
		return strcmp($aValue, $bValue);
	}

	/**
	 * Build a swatch Varien_Object instance with all available information as attributes.
	 *
	 * @param Mage_Catalog_Model_Product $product
	 * @param int $width
	 * @param int $height
	 * @return Varien_Object
	 */
	protected function _getSwatchDataFromProduct(Mage_Catalog_Model_Product $product, $width, $height)
	{
		$swatch = false;
		$image = $product->getBcpSwatchImage();

        if ($image)
		{
			$swatch = new Varien_Object(array(
				'product_id' => $product->getId(),
				'product' => $product,
				'name' => $product->getName(),
				'sku' => $product->getSku(),
				'add_to_cart_url' => $this->getAddToCartUrl($product),
				'is_in_stock' => $product->getIsInStock(),
				'is_salable'  => $product->getIsSalable(),
				'is_saleable' => $product->getIsSalable(), // british english alias to is_salable
			));

			if ($additionalAttributes = Mage::helper('bcp_swatches')->getConfig('extra_product_attributes'))
			{
				foreach (explode(',', $additionalAttributes) as $attributeCode)
				{
					$attributeCode = trim($attributeCode);
					$swatch->setData($attributeCode, $product->getDataUsingMethod($attributeCode));
				}
			}

			if ((substr($image, 0, 7) == 'http://') || (substr($image, 0, 8) == 'https://'))
			{
				$imagePath = '';
				$url = $image;
			}
			else
			{
				/*
				 * If a width and height is specified scale the swatch image and generate the image url.
				 * If width AND height are set to 0 then do not generate the image url, do not scale the image.
				 * The theme builder can make her own call to the image helper as needed using the image path.
				 */

				$url = '';
				if (($image != 'no_selection' && $image != Mage::helper('bcp_swatches')->getSwatchesImageDir()) && ($width != 0 || $height != 0))
				{
					$imageObj = Mage::helper('catalog/image')->init($product, 'swatches', $image);
					if ($width && $height)
					{
						$imageObj->resize($width, $height);
					}
					elseif ($width && ! $height)
					{
						$imageObj->resize($width);
					}
					elseif (! $width && $height)
					{
						$imageObj->resize(null, $height);
					}
					$url = (string) $imageObj;
				}
			}


			$swatch->setImagePath($image);
			$swatch->setImageUrl($url);

			if (Mage::helper('bcp_swatches')->getConfig('load_simple_product_prices'))
			{
				$swatch->setPrice($product->getPrice());
				$swatch->setFinalPrice($product->getFinalPrice());

				$swatch->setFormattedPrice(Mage::helper('core')->currency($swatch->getPrice(), true, false));
				$swatch->setFormattedFinalPrice(Mage::helper('core')->currency($swatch->getFinalPrice(), true, false));
			}
		}
		return $swatch;
	}

	/**
	 * Get the simple products from the configurable products with the needed attributes.
	 *
	 * @param Mage_Catalog_Model_Product $product
	 * @return Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection
	 */
	protected function _getSimpleProductCollection(Mage_Catalog_Model_Product $product)
	{
		$collection = $product->getTypeInstance(true)->getUsedProductCollection($product)
			->addAttributeToSelect(array(
				'name',
				'thumbnail',
				'small_image',
				'image',
				'bcp_swatch_image_source',
				'bcp_swatch_image_url',
				'is_in_stock',
			))
			->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
		;
		foreach ($product->getTypeInstance(true)->getConfigurableAttributes($product) as $attribute)
		{
			$collection->addAttributeToSelect($attribute->getProductAttribute()->getAttributeCode());
		}

		if (Mage::helper('bcp_swatches')->getConfig('load_simple_product_prices'))
		{
			$collection->addAttributeToSelect(array(
				'price',
				'special_price',
				'special_from_date',
				'special_to_date',
			));
		}

		if ($additionalAttributes = Mage::helper('bcp_swatches')->getConfig('extra_product_attributes'))
		{
			foreach (explode(',', $additionalAttributes) as $attributeCode)
			{
				$attributeCode = trim($attributeCode);
				$collection->addAttributeToSelect($attributeCode);
			}
		}

		return $collection;
	}

	public function getAddToCartUrl(Mage_Catalog_Model_Product $simpleProduct)
	{
		$url = '';
		$product = $this->getProduct();

		if ($product->isConfigurable())
		{
			$continueShoppingUrl = Mage::getUrl('*/*/*', array(
				'_current' => true,
				'_use_rewrite' => true,
				'_query' => array(
					DerModPro_BCP_Model_Observer::SELECTION_VAR_NAME => $simpleProduct->getId(),
				),
			));
			$params = array(
					Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED => Mage::helper('core')->urlEncode($continueShoppingUrl),
					'product' => $product->getId(),
					'_query' => array(),
			);

			foreach ($product->getTypeInstance(true)->getConfigurableAttributes($product) as $attribute)
			{
				$key = sprintf('super_attribute[%s]', $attribute->getProductAttribute()->getId());
				$params['_query'][$key] = $simpleProduct->getData($attribute->getProductAttribute()->getAttributeCode());
			}
			$url = Mage::helper('checkout/cart')->getAddUrl($product, $params);
		}
		return $url;
	}

	/**
	 * Only return something if an configurable product is being viewed.
	 *
	 * @return string
	 */
	protected function _toHtml()
	{
		/*
		 * Paranoia: only in the layout xml is messed up this could be triggered...
		 */
		if (! $this->getProduct() || ! $this->getProduct()->isConfigurable())
		{
			return '';
		}
		return parent::_toHtml();
	}

	/**
	 * Return the model of the currently viewed product
	 *
	 * @return Mage_Catalog_Model_Product
	 */
	public function getProduct()
	{
		return Mage::registry('current_product');
	}
}