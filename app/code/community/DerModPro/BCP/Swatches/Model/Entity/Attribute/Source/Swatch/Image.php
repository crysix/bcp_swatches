<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_BCP_Swatches
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_BCP_Swatches_Model_Entity_Attribute_Source_Swatch_Image
	extends Mage_Eav_Model_Entity_Attribute_Source_Config
{
	/**
	 * The path to the options to use in the config xml
	 *
	 * @var string
	 */
	protected $_configNodePath = 'global/bcp_swatches/source/swatch_image';

	/**
	 * Retrieve options array
	 *
	 * @return array
	 */
	public function getAllOptions()
	{
		if (is_null($this->_options))
		{
			$options = parent::getAllOptions();
			array_unshift($options, array(
				'label' => Mage::helper('bcp_swatches')->__('Use Configuration Setting'),
				'value' => '',
			));

			$options[] = array(
				'label' => Mage::helper('bcp_swatches')->__('Manual Setting'),
				'value' => 'url',
			);

			$this->_options = $options;
		}
		return $this->_options;
	}

	/**
	 * Bugfix for Magento 1.3 - do not return the option array entry, only the label.
	 *
	 * @param mixed $value
	 * @return string
	 */
	public function getOptionText($value)
	{
		$option = parent::getOptionText($value);
		if (is_array($option) && isset($option['label']))
		{
			$option = $option['label'];
		}
		return $option;
	}
}