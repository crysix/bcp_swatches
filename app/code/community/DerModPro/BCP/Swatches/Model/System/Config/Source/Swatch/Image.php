<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_BCP_Swatches
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_BCP_Swatches_Model_System_Config_Source_Swatch_Image
{
	/**
	 * The path to the options to use in the config xml
	 *
	 * @var string
	 */
	protected $_configNodePath = 'global/bcp_swatches/source/swatch_image';

	/**
	 * Cache the options in memory
	 *
	 * @var array
	 */
    protected $_options;

	/**
	 * Return the available swatch image source options
	 *
	 * @return array
	 */
    public function toOptionArray()
    {
        if (is_null($this->_options)) {
            $this->_options = array(array(
				'label' => Mage::helper('bcp_swatches')->__('No Default'),
				'value' => '',
			));
            if ($this->_configNodePath) {
                $rootNode = Mage::getConfig()->getNode($this->_configNodePath);
            }
            if (!$rootNode) {
                throw Mage::exception('Mage_Eav', Mage::helper('eav')->__('Failed to load node %s from config.', $this->_configNodePath));
            }
            $options = $rootNode->children();
            if (empty($options)) {
                throw Mage::exception('Mage_Eav', Mage::helper('eav')->__('No options found in config node %s', $this->_configNodePath));
            }
            foreach ($options as $option) {
                $this->_options[] = array(
                    'value' => (string)$option->value,
                    'label' => (string)$option->label
                );
            }
        }

        return $this->_options;
    }
}