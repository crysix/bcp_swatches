/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_BCP_Swatches
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

=== ABOUT ===
This extension enables you to display an image for every simple product assigned to a configurable product.
A click on the swatch image triggers the same actions as if the customer had selected the matching options, i.e. the images and the other page sections are updated.


=== FEATURES ===
- Swatch Images can be freely configured
- Easy integration into existing themes
- Uses the magento block cache (if configured)


=== INSTALLATION ===
BCP_Swatches extends the BCP Module, it is no replacement. BCP_Swatches enhances the BCP module, so you need to install BOTH.

FIRST install the BCP extension by unpacking the archive in the magento directory. Then clear the magento cache and
rebuild the flat catalog tables.

SECOND unpack the BCP_Swaches ZIP archive in the magento directory. Then clear the magento cache again. You don't need to
rebuild the flat catalog tables for the swatches extension.

THIRD you need to include the following code in the catalog/product/view.phtml template from your theme:

			<?php echo $this->getChildHtml('swatches') ?>

Please make sure you don't place it in a html node that is updated by the BCP module when a simple product is selected. Otherwise you
are free to choose any location that fits the design of your theme.
You can also check out the sample of a more advanced swatches template we included by using $this->getChildHtml('swatches_expanded').
If you want to try out BCP Swatches with the Magento default theme, we suggest you add the swatches code around line 86, below
<?php echo $this->getChildHtml('other');?>. If you decide to try the extended swatches template with the default theme, add the
swatches code below the product image box, around line 96;

You can customize the display of the swatches block by copying the template
				template/dermodpro/bcpswatches/catalog/product/view/swatches.phtml
into your theme and modifying it as required. The same applies to the css for the block, which can be found in the file
				skin/frontend/default/default/dermodpro/bcpswatches/css/swatches.css
The expanded swatches sample template can be found in template/dermodpro/bcpswatches/catalog/product/view/swatches_expanded.phtml.

Please do not modify the original files, otherwise your changes will be overwritten when you upgrade the extension.
Copy the files into your theme directory tree and then modify the copies.

The first step after the installation should be to go to the system configuration and configure the extension as you need.


=== CONFIGURATION ===
The configuration is found under System > Configuration > Catalog > Better Configurable Product Settings
in an extra section "Swatches Setting". The image source setting can be set for each simple product, so
you may choose to set a default in the system configuration and then override it per product where needed.

All BCP_Swatches System Configuration options:

= Default image to use in the simple product swatch.
  The available options are
  - No Default
  - Thumbnail
  - Small Image
  - Image
  - First image in the products media gallery
  - Last image in the products media gallery
  This option can be overridden per product.

= Swatch Image Width
  The width to use for the swatch images. See the comments in the template dermodpro/bcpswatches/catalog/product/view/swatches.phtml
  for advanced options.

= Swatch Image Height
  The height to use for the swatch images. See the comments in the template dermodpro/bcpswatches/catalog/product/view/swatches.phtml
  for advanced options.

= Additional Product Attributes to load on Swatches
  This is an advanced configuration option that you probably will not need. If you want any attributes that aren't loaded on the swatch
  products by default you can specify the attribute codes here (comma seperated). They will be assined to each the swatch object in the template.

= Sort swatch images by
  Specify an optional sort order for the swatch images.

= Load Prices for simple products
  If you want to display the price for each option in the swatches block, set this option to "Yes". Otherwise
  its best to disable this feature for better performance.

= Swatches Block Cache TTL
  You can specify the number of seconds the block should remain in the magento cache. If you have a very dynamic
  site with product prices that change quickly, you can disable caching by setting this value to 0 (zero).


=== CHOOSING A SWATCHES IMAGE SOURCE ===
There are several sources you can choose for your swatch images.

One option is to use an image from the simple products media gallery (image, small_image, thumbnail, first image or the last image).
If you do not want the swatch image to show in the list of product images on the frontend, check the "Exclude" option in the product
configuration.

Another possibility is to manually specify a swatch image. Use this option e.g. if you want your products to share swatch images, and do not
want to upload the same image for every product again and again. Put the image file to use in the media/bcp_swatches/ directory,
and enter the image name in the field "Swatches Image Path" (in the manage products interface).
Then select "Manual Setting" for the "Swatches Image Source" setting.

If you prefer you can also place a http:// or https:// URL that points to the image you want to use into the "Swatches Image Path" field. The URL will be used
as you enter it, the image will no be scaled as files local to the server would be.


=== CUSTOMIZING ===
Please read the comment block in the swatches template file
			template/dermodpro/bcpswatches/catalog/product/view/swatches.phtml
All available options are documented there.


=== SUPPORT ===
Deutsch: http://www.der-modulprogrammierer.de/hilfeseiten/hilfe/bcpswatches-de.html
English: http://www.der-modulprogrammierer.de/hilfeseiten/hilfe/bcpswatches-en.html


=== BUGS ===
If you have ideas for improvements or find bugs, please send them to info@der-modulprogrammierer.de,
with BCP_Swatches as part of the subject.
